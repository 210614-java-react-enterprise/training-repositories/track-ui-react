import React from "react";
import "./App.css";
// import HelloWorld from './components/hello-world-component';
// import HelloWorldFunctional from './components/hello-world-functional-component';
import NavComponent from "./components/common/nav-component";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import TrackDirectory from "./components/directory/directory";
import NewTrack from "./components/new-track/new-track";
import LogIn from "./components/login/login";
import Container from "react-bootstrap/Container";

function App() {
  return (
    <Router>
      <NavComponent></NavComponent>
      <Container>
        <Switch>
          <Route path="/directory">
            <TrackDirectory></TrackDirectory>
          </Route>
          <Route path="/new">
            <NewTrack></NewTrack>
          </Route>
          <Route path="/login">
            <LogIn></LogIn>
          </Route>
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
