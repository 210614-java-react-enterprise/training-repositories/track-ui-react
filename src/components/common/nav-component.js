import Button from "react-bootstrap/Button";
import Nav from "react-bootstrap/Nav";
// import { Nav } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function NavComponent() {
  return (
    <Nav>
        <Button>Music Directory</Button>
        <Nav.Item>
            <Nav.Link as={Link} to="/">Home</Nav.Link>
        </Nav.Item>
        <Nav.Item>
            <Nav.Link as={Link} to="/directory">All Tracks</Nav.Link>
        </Nav.Item>
        <Nav.Item>
            <Nav.Link as={Link} to="/new">Add a New Tracks</Nav.Link>
        </Nav.Item>
        <Nav.Item>
            <Nav.Link as={Link} to="/login">Log In</Nav.Link>
        </Nav.Item>
    </Nav>
  );
}
