
export default function GenericTableRow(props){
    const recordValues = Object.values(props.record);
    return (<tr>{ recordValues.map((value,index)=><td key={index}>{value}</td>)}</tr>);

}