import Table from "react-bootstrap/Table";
import GenericTableRow from "./generic-table-row";

export default function GenericTable(props){
    if(props.records && props.records.length > 0) {
        const objectKeys = Object.keys(props.records[0]);

        return (
            <Table>
                <thead>
                    <tr>{objectKeys.map(objectKey=><th key={objectKey}>{objectKey}</th>)}</tr>
                </thead>
                <tbody>
                    {props.records.map(record=><GenericTableRow key={record.id} record={record}></GenericTableRow>)}
                </tbody>
            </Table>
        )
    } else {
        return <p>No data to show.</p>;
    }
    
}