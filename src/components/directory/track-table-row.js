export default function TrackTableRow(props) {
    console.log(props.record);
  return (
    <tr>
      <td>{props.record.name}</td>
      <td>{props.record.artist}</td>
      <td>{props.record.releaseYear}</td>
    </tr>
  );
}
