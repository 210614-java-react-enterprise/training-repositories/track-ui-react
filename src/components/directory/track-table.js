import { Table } from "react-bootstrap";
import TrackTableRow from "./track-table-row";
import GenericTable from "../common/generic-table";

export default function TrackTable(props) {
  const tracks = props.records;
  console.log(tracks);
  /*
  return (
    <Table>
      <thead>
        <tr>
          <th>Name</th>
          <th>Artist</th>
          <th>Year</th>
        </tr>
      </thead>
      <tbody>
        {tracks.map((track) => <TrackTableRow key={track.id} record={track}></TrackTableRow>)}
      </tbody>
    </Table>
  );
  */
    return <GenericTable records={tracks}></GenericTable>
}
