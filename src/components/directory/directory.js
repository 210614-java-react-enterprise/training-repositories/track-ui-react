import axios from "axios";
import React, { useEffect, useState } from "react";
import TrackTable from "./track-table";

export default function TrackDirectory(props){

    const [tracks, setTracks] = useState([]);

    // useEffect with an empty dependency array is equivalent to componentDidMount 
    useEffect(()=>{
        console.log("directory has been rendered");
        axios.get("http://34.239.1.19:8082/tracks", {headers: {"Authorization":"auth-token"}})
            .then(resp=>{
                setTracks(resp.data);
            })
    },[]);


    /* make a request to our server for some data 
    const tracks = [{
        id:1,
        name: "Rocky Raccoon",
        artist: "The Beatles",
        releaseYear:1968
    }, {
        id: 2,
        name: "Stairway to Heaven",
        artist: "Led Zeppelin",
        releaseYear:1971
    }, {
        id: 3,
        name: "Strawberry Fields Forever",
        artist: "The Beatles",
        releaseYear:1967
    }];
    */

    return <TrackTable records={tracks} ></TrackTable>
}

