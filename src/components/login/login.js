import axios from "axios";
import { useState } from "react";
import { withRouter } from "react-router-dom";
import LoginForm from "./login-form";


function LogIn(props){

    const [credentials, setCredentials] = useState("");

    const handleChange = (e)=>{
        const {value,name} = e.target;
        setCredentials({...credentials, [name]:value});
    }

    const handleSubmit = (e)=>{
        e.preventDefault();
        // param=value&param2=value2...
        const requestBody = `username=${credentials.username}&password=${credentials.password}`;
        console.log(requestBody);
        axios.post("http://34.239.1.19:8082/login",requestBody, {headers:{"Content-Type": "application/x-www-form-urlencoded"}})
            .then(response=>{
                console.log("successful login!!!");
                sessionStorage.setItem("token", response.headers["authorization"]);
                props.history.push("/");
            })
            .catch(err=> console.error("there was an issue logging in :("))
    }

    return <LoginForm onChange={handleChange} onSubmit={handleSubmit}></LoginForm>;
}

export default withRouter(LogIn);
//"file" import = Login 

/*
export function login ... 
export function logout ...
export function auth ...

//"file" import = {login: function(){..}, logout: function(){..}, auth: function(){..}} 
*/