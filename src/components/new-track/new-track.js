import { useState } from "react";
import NewTrackForm from "./new-track-form";
import axios from "axios";
import { withRouter } from "react-router-dom";

function NewTrack(props){

    const [track, setTrack] = useState({});

    /*
        when user changes input, we want the state to be updated
    */
   const handleChangeSwitch = (e)=>{
        // let newTrackValue = e.target.value;
        let newTrack = {...track};
        switch(e.target.name){
            case "name-input":
                newTrack.name = e.target.value;
                break;
            case "artist-input":
                newTrack.artist = e.target.value;
                break;
            case "year-input":
                newTrack.year = e.target.value;
                break;
            default:
        }
        console.log(newTrack);
        setTrack(newTrack);
   }

   const handleChangeDynamic = (e)=>{
       const inputFieldName = e.target.name; // this is the field of track object
       const inputValue = e.target.value; // this is the new value for that field
       const newTrack = {...track};
       newTrack[inputFieldName] = inputValue;
       console.log(newTrack);
       setTrack(newTrack);
   }

   const handleChange = (e)=>{
        // const newTrack = {...track};
        // newTrack[name] = value;
        const {name, value} = e.target; //name and value attribute from the input field which is triggering the event
        let newTrack = {...track, [name]:value};
        setTrack(newTrack);
    }

    /*
        some event (form submission?) sends current state of the track object to the server
    */

    const createNewTrack = (e)=>{
        e.preventDefault();
        //send state to server - this must be defined here
        axios.post("http://34.239.1.19:8082/tracks", track, {headers: {"Authorization":"auth-token"}})
            .then(response=>{
                props.history.push("/directory");
            })
            .catch(err=>console.error(err));
    }


    return <NewTrackForm handleChangeEventListener={handleChange} handleSubmitEventListener={createNewTrack}></NewTrackForm>;
}
/*
    this is a high order function - withRouter takes a component as a parameter
    and returns a component with enhanced functionality
*/
export default withRouter(NewTrack);