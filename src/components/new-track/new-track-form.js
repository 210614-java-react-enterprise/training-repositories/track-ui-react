import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

export default function NewTrackForm(props){

    const printMessage = (e)=>{
        e.preventDefault();
        console.log("form has been submitted!");
    }

    const printChange = (e)=>{
        console.log(e.target.value);
    }

    const createUpdatedTrack = (e)=>{
        const object = {name: e.target.value};
        console.log(object);
    }

    return (<Form>
        <Form.Group>
            <Form.Label>Name:</Form.Label>
            <Form.Control name="name" onChange={props.handleChangeEventListener}></Form.Control>
            <Form.Label>Artist:</Form.Label>
            <Form.Control name="artist" onChange={props.handleChangeEventListener}></Form.Control>
            <Form.Label>Release Year:</Form.Label>
            <Form.Control name="releaseYear" type="number" onChange={props.handleChangeEventListener}></Form.Control>
        </Form.Group>
        <Button variant="info" type="submit" onClick={props.handleSubmitEventListener} >Add New Song!</Button>
    </Form>)
}