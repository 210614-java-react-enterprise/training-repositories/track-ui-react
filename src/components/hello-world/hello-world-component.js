import React from "react";

class HelloWorld extends React.Component {

    constructor(props){
        super(props);
        this.name = props.name;
    }

    render(){
        return <h1>Hello {this.name}!</h1>
    }
}

export default HelloWorld;