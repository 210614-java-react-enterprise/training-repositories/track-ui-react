
function HelloWorldFunctional(props){
    return <h1>Hello {props.name} from my functional component!</h1>
}

export default HelloWorldFunctional;

/*
in App.js
<HelloWorldFunctional name="Carolyn"></HelloWorldFunctional>
*/